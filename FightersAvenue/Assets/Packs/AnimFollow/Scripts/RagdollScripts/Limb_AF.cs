using UnityEngine;
using System.Collections;

namespace AnimFollow
{
	public class Limb_AF : MonoBehaviour
	{
		public readonly int version = 7; // The version of this script

		// This script is distributed (automatically by RagdollControl) to all rigidbodies and reports to the RagdollControl script if any limb is currently colliding.

		RagdollControl_AF ragdollControl;
		string[] ignoreCollidersWithTag;
		private float throwSpeed = 2000f;
		private float volLowRange = .5f;
		private float volHighRange = 1.0f;

		GameObject gameManager;// Update the manager after each collision

		void Start()
		{
			gameManager = GameObject.Find ("GameLogic"); // This is for changing the healthBar.
		}
		void OnEnable()
		{
			ragdollControl = transform.root.GetComponentInChildren<RagdollControl_AF>();

			ignoreCollidersWithTag  = ragdollControl.ignoreCollidersWithTag;

		}

		void OnCollisionEnter(Collision collision)
		{
			if ((collision.transform.name == ragdollControl.sword.name || collision.transform.name == ragdollControl.shield.name)&& collision.transform.IsChildOf(ragdollControl.ragdollRootBone)) // Holding the weapon not hitting who holds hit
			{
				Physics.IgnoreCollision (this.transform.GetComponent<Collider> (), collision.collider);

			}

			bool ignore = false;

			// this is to know wich part was been hit:collision.transform.name 
			// only remove if it was been hit by the enemy's 
	
			//if(!hitArenaColliders(collision) && !collision.transform.IsChildOf(ragdollControl.ragdollRootBone)) // Not colliding the scene and not hitting itself
			//{
				//Debug.Log (collision.transform.name);//who hit me

			//}
			//Debug.Log (this.GetComponent<Collider>());//where hit me

			if (!(collision.transform.name == "pCube1_gro")  && (collision.transform.root != this.transform.root) && !hitArenaColliders(collision)) // Collide with enemy &&!(collision.transform.name == "colloders")
			{
				//takes off from health
				ragdollControl.transform.parent.gameObject.SendMessage("TakeHealth", 0.01f);

				// Create swords and iron sounds
				float vol = Random.Range (volLowRange, volHighRange);
				int index = Random.Range (0, ragdollControl.hitSound.Length);
				ragdollControl.source.PlayOneShot(ragdollControl.hitSound[index],vol);

				foreach (string ignoreTag in ignoreCollidersWithTag)
				{
					if (collision.transform.tag == ignoreTag)
					{
						ignore = true;
						break;
					}
				}

				if (!ignore)
				{
					ragdollControl.numberOfCollisions++;
					ragdollControl.collisionSpeed = collision.relativeVelocity.magnitude;
//					Debug.Log (collision.transform.name + "\nincreasing");
				}
				ragdollControl.bloodClone = Instantiate(ragdollControl.blood, this.GetComponent<Collider>().transform.position, Quaternion.LookRotation(this.GetComponent<Collider>().transform.position)) as ParticleSystem;///
				ragdollControl.shotByBullet = true;
				ragdollControl.bloodClone.transform.parent = this.GetComponent<Collider>().transform;
				ragdollControl.bloodClone.Play();
				Destroy(ragdollControl.bloodClone.gameObject, 1f);

				if (this.GetComponent<Collider>().transform.name.Contains("Head"))
					ragdollControl.shotInHead = true;
				
			}
		}
		void OnCollisionExit(Collision collision)
		{
			bool ignore = false;
			if (!(collision.transform.name == "Terrain") && collision.transform.root != this.transform.root)
			{
				foreach (string ignoreTag in ignoreCollidersWithTag)
				{
					if (collision.transform.tag == ignoreTag)
					{
						ignore = true;
						break;
					}
				}

				if (!ignore)
				{
					ragdollControl.numberOfCollisions--;
	//				Debug.Log (collision.transform.name + "\ndecreasing");
				}
			}
		}

		bool hitArenaColliders(Collision collision)
		{
			if (collision.transform.name != "arenaCollider1" && collision.transform.name != "arenaCollider2" && collision.transform.name != "arenaCollider3" && collision.transform.name != "arenaCollider4")
				return false;
			return true;
		}
	}
}
