﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


namespace AnimFollow
{
	public class GameLogicScript : MonoBehaviour {

		public Transform player1Pos;
		public Transform player2Pos;

		public GameObject[] playerPrefabs;

		public Transform cameraFollow;
		public Camera mainCamera;

		public GameObject characterTag;

		GameObject  player1;
		GameObject player2;

		GameObject CurrentPlayer;

		bool isFighting = true;

		//static GameLogicScript GL;

		public Canvas myCanvas;

		public AudioClip[] sounds;
		private AudioSource source;

		float timer;

		void Awake()
		{
			source = GetComponent<AudioSource>();
			//DontDestroyOnLoad (gameObject);
		}

		// Use this for initialization
		void Start () {
			// Add player 1 to the scene
			player1 = Instantiate (playerPrefabs[StaticsBool.player1Index], player1Pos.position, player1Pos.rotation); // Instantiate acordding to the selected character

			// Change the position of the ragdoll and the master too
			player1.transform.GetChild (0).position = player1Pos.position;
			player1.transform.GetChild (1).position = player1Pos.position;

			// Add player 2 to the scene
			player2 = Instantiate (playerPrefabs[StaticsBool.player2Index], player2Pos.position, player2Pos.rotation); // Instantiate acordding to the selected character

//			GameObject.Find (player1.transform.GetChild (1).GetComponent<RagdollControl_AF>().shield).gameObject.SetActive (true);
			//string st =player1.transform.GetChild (1).GetComponent<RagdollControl_AF>().shield;
			//player1.transform.Find (st).gameObject.SetActive(true);

			// Change the position of the ragdoll and the master too
			player2.transform.GetChild (0).position = player2Pos.position;
			player2.transform.GetChild (1).position = player2Pos.position;
			
			//Player 2 in the start cant move, only player 1 
			//player2.transform.GetChild (0).GetComponent<IK_Plane>().enabled = false;
			player2.transform.GetChild (0).GetComponent<IK_Plane>().setAtatackBool(false);
			player1.transform.GetChild (0).GetComponent<IK_Plane>().setAtatackBool(true);

			player2.transform.GetChild (0).GetComponent<FighterController>().enabled = false;
			player2.transform.GetChild (0).GetComponent<PlayerMovement_AF>().enabled = false;

			// Add name, health and manaBar for both of the players
			myCanvas.transform.GetChild (0).GetComponent<FollowPlayerAbove> ().player = player1;
			myCanvas.transform.GetChild (0).GetChild (0).GetComponent<Text>().text = "player1";

			myCanvas.transform.GetChild (1).GetComponent<FollowPlayerAbove> ().player = player2;
			myCanvas.transform.GetChild (1).GetChild (0).GetComponent<Text>().text = "player2";

			myCanvas.transform.GetChild (2).gameObject.SetActive (false);
			myCanvas.transform.GetChild (3).gameObject.SetActive (false);


			CurrentPlayer = player1;


			// Set the shield and the sword active:
			player1.transform.GetChild (1).GetComponent< RagdollControl_AF> ().shield.gameObject.SetActive (true);
			player1.transform.GetChild (1).GetComponent< RagdollControl_AF> ().sword.gameObject.SetActive (true);

			player2.transform.GetChild (1).GetComponent< RagdollControl_AF> ().shield.gameObject.SetActive (true);
			player2.transform.GetChild (1).GetComponent< RagdollControl_AF> ().sword.gameObject.SetActive (true);

			// Switch the panel
			player2.gameObject.GetComponent<HealthBar> ().healthBar = myCanvas.transform.GetChild (1).GetChild (1).GetComponent<Slider> ();
			player2.gameObject.GetComponent<HealthBar> ().ManaBar = myCanvas.transform.GetChild (1).GetChild (2).GetComponent<Slider> ();
			player1.gameObject.GetComponent<HealthBar> ().healthBar = myCanvas.transform.GetChild (0).GetChild (1).GetComponent<Slider> ();
			player1.gameObject.GetComponent<HealthBar> ().ManaBar = myCanvas.transform.GetChild (0).GetChild (2).GetComponent<Slider> ();


		}

		void Update(){

			characterTag.transform.position = new Vector3 (CurrentPlayer.transform.position.x, CurrentPlayer.transform.position.y + 2.2f, CurrentPlayer.transform.position.z);

			if (isFighting) {
				float avgX = (player1.transform.GetChild (0).transform.position.x + player2.transform.GetChild (0).transform.position.x) / 2 - 2f;
				float avgZ = (player1.transform.GetChild (0).transform.position.z + player2.transform.GetChild (0).transform.position.z) / 2;

				cameraFollow.position = new Vector3 (avgX, cameraFollow.position.y, avgZ);
				//CameraFollow.rotation = CurrentPlayer.transform.rotation;
				
				if (CurrentPlayer.gameObject.GetComponent<HealthBar> ().isManaEmpty ()) { // This player have finished its turn

					if (CurrentPlayer == player1) {

						myCanvas.transform.GetChild (2).GetChild(0).GetComponent<Text>().text = "Its player's 2 Turn";
						source.PlayOneShot(sounds[0],1.0f); // Make switching turns sound

						myCanvas.transform.GetChild (2).gameObject.SetActive (true);
						Invoke ("clearTurnsScreen", 1.0f);

						player2.transform.GetChild (0).GetComponent<IK_Plane>().setAtatackBool(false);

						player1.gameObject.GetComponent<HealthBar> ().HealMana ();
						// Disable this player's scripts
						//CurrentPlayer.transform.GetChild (0).GetComponent<IK_Plane>().enabled = false;
						CurrentPlayer.transform.GetChild (0).GetComponent<IK_Plane> ().setAtatackBool (false);
						CurrentPlayer.transform.GetChild (0).GetComponent<FighterController> ().enabled = false;
						CurrentPlayer.transform.GetChild (0).GetComponent<PlayerMovement_AF> ().enabled = false;


						// Switch to the other player
						CurrentPlayer = player2;
						// Switch the camera to default state
						//mainCamera.GetComponent<cameraObserver>().InitPos();
						//mainCamera.transform.rotation +=new Quaternion(0,20,0);
					} else {

						myCanvas.transform.GetChild (2).GetChild(0).GetComponent<Text>().text = "Its player's 1 Turn";
						myCanvas.transform.GetChild (2).gameObject.SetActive (true);
						source.PlayOneShot(sounds[0],1.0f); // Make switching turns sound
						Invoke ("clearTurnsScreen", 1.0f);



						player1.transform.GetChild (0).GetComponent<IK_Plane>().setAtatackBool(false);

						player2.gameObject.GetComponent<HealthBar> ().HealMana ();
						// Disable this player's scripts
						//CurrentPlayer.transform.GetChild (0).GetComponent<IK_Plane>().enabled = false;
						CurrentPlayer.transform.GetChild (0).GetComponent<IK_Plane> ().setAtatackBool (false);
						CurrentPlayer.transform.GetChild (0).GetComponent<FighterController> ().enabled = false;
						CurrentPlayer.transform.GetChild (0).GetComponent<PlayerMovement_AF> ().enabled = false;

						// Switch the panel
						//gameObject.GetComponent<HealthBar> ().healthBar = myCanvas.transform.GetChild (0).GetChild (1).GetComponent<Slider> ();
						//gameObject.GetComponent<HealthBar> ().ManaBar = myCanvas.transform.GetChild (0).GetChild (2).GetComponent<Slider> ();
					
						// Switch to the other player
						CurrentPlayer = player1;
						// Switch the camera to default state
						//mainCamera.GetComponent<cameraObserver>().InitPos();
						//mainCamera.transform.rotation -=new Quaternion(0,20,0);
					}


					//CurrentPlayer.transform.GetChild (0).GetComponent<IK_Plane> ().setAtatackBool (true);


					// Switch the camera
				
				}

				if (player1.gameObject.GetComponent<HealthBar> ().isHealthEmpty () || player2.gameObject.GetComponent<HealthBar> ().isHealthEmpty ()) { // Player lose
					// Game over, player 1\2 wins and we go back to the menu

					if (CurrentPlayer == player1) {  // Player 2 wins
					
						player2.transform.GetChild (0).GetComponent<FighterController> ().isLose = true;
						//player2.transform.GetChild (0).GetComponent<FighterController> ().isWin= true;

						player1.transform.GetChild (0).GetComponent<IK_Plane> ().enabled = false;
						player2.transform.GetChild (0).GetComponent<IK_Plane> ().enabled = false;

						isFighting = false;

						myCanvas.transform.GetChild (3).GetChild(0).GetComponent<Text>().text = "The fight is over\nWinner is player 2";
						source.PlayOneShot(sounds[1],0.5f); // Make fight finished sounds



					} else { // Player 1 wins
						player1.transform.GetChild (0).GetComponent<FighterController> ().isLose = true;
						//player1.transform.GetChild (0).GetComponent<FighterController> ().isWin= true;

						player1.transform.GetChild (0).GetComponent<IK_Plane> ().enabled = false;
						player2.transform.GetChild (0).GetComponent<IK_Plane> ().enabled = false;

						isFighting = false;

						myCanvas.transform.GetChild (3).GetChild(0).GetComponent<Text>().text = "The fight is over\nWinner is player 1";
						source.PlayOneShot(sounds[1],0.5f); // Make fight finished sound



					}
					myCanvas.transform.GetChild (3).gameObject.SetActive (true); // Show game over panel

				}
			}
		}
		void FixedUpdate () {

			if (Input.GetKey (KeyCode.Escape))
				SceneManager.LoadScene ("MainMenu");
			
				
			// Keep in the same posiotion to ease the attack
			/*if (player1.transform.GetChild(0).position.x != player1Pos.position.x) {
				player1.transform.GetChild(0).position = player1Pos.position;
			}

			if (player2.transform.GetChild(0).position.x != player2Pos.position.x) {
				player2.transform.GetChild(0).position = player2Pos.position;
			}*/

	}

		public void PressedOk() // go back to menu after the fight is over and user pressed ok
		{
			SceneManager.LoadScene ("MainMenu");
		}

		void clearTurnsScreen()
		{
			myCanvas.transform.GetChild (2).gameObject.SetActive (false);
			CurrentPlayer.transform.GetChild (0).GetComponent<IK_Plane>().setAtatackBool(true);
			//player2.transform.GetChild (0).GetComponent<IK_Plane>().setAtatackBool(true);
			CurrentPlayer.transform.GetChild (0).GetComponent<FighterController> ().enabled = true;
			CurrentPlayer.transform.GetChild (0).GetComponent<PlayerMovement_AF> ().enabled = true;

		}
	}

}