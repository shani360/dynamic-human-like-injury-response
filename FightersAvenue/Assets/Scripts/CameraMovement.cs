﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {

	const float Y_ANGLE_MIN = 1f;
	const float Y_ANGLE_MAX = 50f;

	Rect guiBox = new Rect(180, 5, 160, 140);

	public Transform lookAt;
	public Transform camTransform;
	Camera cam;
	float distance=5.0f;
	float currentX=5.0f;
	float currentY=5.0f;
	float sensivityX=4f;
	float sensivityY = 1f;

	void Start()
	{
		camTransform = transform;
		cam = Camera.main;
	}

	void Update()
	{
		if(Input.GetKey (KeyCode.Mouse2)) //mouse middle button
		{
			currentX += Input.GetAxis ("Mouse X");
			currentY += Input.GetAxis ("Mouse Y");

			currentY = Mathf.Clamp (currentY, Y_ANGLE_MIN, Y_ANGLE_MAX);
		}

		if (Input.GetAxis ("Mouse ScrollWheel") > 0) 
		{
			GetComponent<Camera> ().fieldOfView--;
		}
		if (Input.GetAxis ("Mouse ScrollWheel") < 0) 
		{
			GetComponent<Camera> ().fieldOfView++;
		}
	}

	void LateUpdate()
	{
		Vector3 dir = new Vector3 (0, 0, -distance);
		Quaternion rotation = Quaternion.Euler (currentY, currentX, 0);
		camTransform.position = lookAt.position + rotation *dir;//Putting a camera on top of the player,then we apply the rotation and times the direction
		camTransform.LookAt(lookAt.position);

	}

	void OnGUI ()
	{
		GUI.Box(guiBox, "Scroll to zoom-in or out\nmouse middle button camera movement\nBend='Q'\n'Sneaking='Ctrl'+W\nJump='Space'\nDeffence='E'\nTest1='J'\nTest2='K'\nTest3='L'");
	}
}
