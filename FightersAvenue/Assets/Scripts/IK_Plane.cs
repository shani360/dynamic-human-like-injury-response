﻿using System.Collections;
using UnityEngine;

static class Constnts
{
	public const float DISTANCE = 0.4f; // Distance between the character and the plain
	public const float MOVEMENT_SPEED=5f;
	public const float ROTATE_BACK_FACTOR = 0.99f; // How fast the plane go back to its place

}

public class IK_Plane: MonoBehaviour {

	public Transform plane;

	public float rightHandClampY = 0.7f;
	public float rightHandClampX = 0.7f;

	public float leftHandClamp;

	//public Transform hintLeft;
	//public Transform targetLeft;

	public GameObject Coil;
	private IK_Coil leftCoil;
	private IK_Coil rightCoil;

	public GameObject PlaneCube;
	private GameObject planeCube;

	private Vector3 positionLeft;
	private Vector3 positionRight;

	private Vector3 relativeBallPos;
	public Transform characterPos;

	private Animator anim;
	private bool attackFlag = true;

	GameObject gameManager;

	public AudioClip[] moveSounds;
	private AudioSource source;

	public float takeMana=0.001f;

	void Awake(){

		plane = new GameObject().transform;
		source = GetComponent<AudioSource>();

	}
	// Use this for initialization
	void Start () {

		gameManager = GameObject.Find ("GameLogic"); // For controlling the ManaBar

		plane.position = new Vector3(0,1,0);
		plane.rotation = Quaternion.identity;

		planeCube = Instantiate (PlaneCube);
		planeCube.transform.position = plane.position;
		planeCube.transform.rotation = plane.rotation;
		planeCube.GetComponent<MeshRenderer> ().enabled = false;


		Vector3 leftTarget = this.transform.position + this.transform.forward * Constnts.DISTANCE + new Vector3(0,1,0) + this.transform.rotation * plane.rotation *new Vector3(0,0,0);// positionLeft;
		Vector3 rightTarget = this.transform.position - this.transform.forward * Constnts.DISTANCE + new Vector3(0,1,0) + this.transform.rotation * plane.rotation *new Vector3(0,0,0);//* positionRight;

		leftCoil = Instantiate (Coil,leftTarget,Quaternion.identity).GetComponent<IK_Coil>();
		rightCoil = Instantiate (Coil,rightTarget,Quaternion.identity).GetComponent<IK_Coil>();

		anim = GetComponent<Animator> ();
	}

	// Update is called once per frame
	void Update () {
		if(attackFlag)
			mouseMov ();
	}

	void FixedUpdate()
	{

		planeCube.GetComponent<Rigidbody>().position = plane.position + this.transform.position+this.transform.forward*Constnts.DISTANCE;
		planeCube.GetComponent<Rigidbody>().rotation = this.transform.rotation * plane.rotation ;


		Vector3 leftTarget = this.transform.position + this.transform.forward * Constnts.DISTANCE + new Vector3 (0, 1, 0) + this.transform.rotation * plane.rotation* positionLeft;
		Vector3 rightTarget = this.transform.position + this.transform.forward * Constnts.DISTANCE + new Vector3(0,1,0) + this.transform.rotation * plane.rotation * positionRight;

		float k = 40;
		float damp = 0.5f;

		// Coil force on the invisible rigidbodies
		/*if (attackFlag) {
            leftCoil.rB.AddForce (k * (leftTarget - leftCoil.rB.position) - damp * leftCoil.rB.velocity);
            rightCoil.rB.AddForce (k * (rightTarget - rightCoil.rB.position) - damp * rightCoil.rB.velocity);
        
 
        // The plane is slowly going back to its origion position
            if (Input.GetKey (KeyCode.LeftControl) || Input.GetKey (KeyCode.RightControl))
                plane.rotation = Quaternion.Slerp(Quaternion.identity, plane.rotation, Constnts.ROTATE_BACK_FACTOR);
        }*/

		return;
	}

	public void setAtatackBool(bool isAttack)
	{
		attackFlag = isAttack;
	}
	void mouseMov()
	{
		// Moving takes mana off
		if ( Input.GetKeyDown (KeyCode.A) || Input.GetKey (KeyCode.W) || Input.GetKey (KeyCode.D) || Input.GetKey (KeyCode.S) || Input.GetKey (KeyCode.LeftArrow
		) || Input.GetKey (KeyCode.RightArrow)|| Input.GetKey (KeyCode.UpArrow) || Input.GetKey (KeyCode.DownArrow)) { 
			this.transform.parent.gameObject.SendMessage("TakeMana", takeMana);// Walking takes off from manaBar

		}
		//if ((Input.GetKey (KeyCode.Mouse0) && !Input.GetKey (KeyCode.Mouse1)) || ((Input.GetKey (KeyCode.Mouse1) && !Input.GetKey (KeyCode.Mouse0)))) // Only one button pressed
		//{
		if (Input.GetKey (KeyCode.Mouse0) && !Input.GetKey (KeyCode.Mouse1)) {
			relativeBallPos = leftCoil.transform.position - characterPos.position;
			relativeBallPos = Quaternion.AngleAxis(Input.GetAxis ("Mouse X")  * Constnts.MOVEMENT_SPEED, Vector3.up) * relativeBallPos;
			leftCoil.transform.position = relativeBallPos + characterPos.position;  

			// Bounds for the hand movenent
			/*Vector3 pos = leftCoil.transform.position;
			pos.x = Mathf.Clamp(pos.x,leftCoil.transform.position.x-0.2f,leftCoil.transform.position.x+0.2f);
			pos.y = Mathf.Clamp(pos.y, leftCoil.transform.position.y-1f, leftCoil.transform.position.y+1f);
			pos.z = Mathf.Clamp(pos.z, leftCoil.transform.position.z -0.2f, leftCoil.transform.position.z+0.2f);
                leftCoil.transform.position = pos;*/

			//leftCoil.transform.position += (leftCoil.transform.position - characterPos.position).normalized * Input.GetAxis ("Mouse X") * Constnts.MOVEMENT_SPEED;
			this.transform.parent.gameObject.SendMessage("TakeMana", takeMana);// Moving the hands takes off from manaBar
			source.PlayOneShot(moveSounds[1],0.5f); // Play left hand movement sound
		}
		if (Input.GetKey (KeyCode.Mouse1) && !Input.GetKey (KeyCode.Mouse0)) {
			relativeBallPos = rightCoil.transform.position - characterPos.position;
			relativeBallPos = Quaternion.AngleAxis (Input.GetAxis ("Mouse X")  * Constnts.MOVEMENT_SPEED, Vector3.up) * relativeBallPos;
			rightCoil.transform.position = relativeBallPos + characterPos.position;

			// Bounds for the hand movenent
			/*Vector3 pos = rightCoil.transform.position;
                pos.x = Mathf.Clamp(pos.x,pos.x-0.99f,pos.x+0.99f);
                pos.y = Mathf.Clamp(pos.y, pos.y-0.99f, pos.y+0.99f);
                pos.z = Mathf.Clamp(pos.z, pos.z -0.99f, pos.z+0.99f);
                rightCoil.transform.position = pos;*/
			//rightCoil.transform.position -= (rightCoil.transform.position - characterPos.position).normalized * Input.GetAxis ("Mouse X") * Constnts.MOVEMENT_SPEED;

			this.transform.parent.gameObject.SendMessage("TakeMana", takeMana);// Moving the hands takes off from manaBar
			source.PlayOneShot(moveSounds[1],0.5f); // Play left hand movement sound
		}

		//}

		if (Input.GetKey (KeyCode.Mouse1) && Input.GetKey (KeyCode.LeftControl))//&& Input.GetKey (KeyCode.Mouse0))  // Both buttons pressed:
		{
			//leftCoil.transform.position += (leftCoil.transform.position - characterPos.position).normalized * Input.GetAxis ("Mouse X") * Constnts.MOVEMENT_SPEED;
			//rightCoil.transform.position -= (rightCoil.transform.position - characterPos.position).normalized * Input.GetAxis ("Mouse X") * Constnts.MOVEMENT_SPEED;

			//source.PlayOneShot(moveSounds[2],0.5f); // Play left hand movement sound

			if (Input.GetKey (KeyCode.Mouse0) ) // Left button pressed:
			{
				relativeBallPos = leftCoil.transform.position - characterPos.position;  
				//rotate relativeBallPos around Vector3.Cross(relativeBallPos, Vector3.Up).normalized with an angle of mouseDy * movementSpeed
				relativeBallPos = Quaternion.AngleAxis(Input.GetAxis ("Mouse Y") * Constnts.MOVEMENT_SPEED, Vector3.Cross(relativeBallPos, Vector3.up).normalized) * relativeBallPos;
				leftCoil.transform.position = relativeBallPos + characterPos.position;  

				// Bounds for the hand movenent
				/*Vector3 pos = leftCoil.transform.position;
                pos.x = Mathf.Clamp(pos.x,   -0.3f, 0.3f);
                pos.y = Mathf.Clamp(pos.y,  -0.3f, 0.3f);
                leftCoil.transform.position = pos;*/
			}
			if (Input.GetKey (KeyCode.Mouse1)) // Right button pressed:
			{
				relativeBallPos = rightCoil.transform.position - characterPos.position;  
				//rotate relativeBallPos around Vector3.Cross(relativeBallPos, Vector3.Up).normalized with an angle of mouseDy * movementSpeed
				relativeBallPos = Quaternion.AngleAxis(Input.GetAxis ("Mouse Y") * Constnts.MOVEMENT_SPEED, Vector3.Cross(relativeBallPos, Vector3.up).normalized) * relativeBallPos;
				rightCoil.transform.position = relativeBallPos + characterPos.position;

				// Bounds for the hand movenent
				/*Vector3 pos = rightCoil.transform.position;
                pos.x = Mathf.Clamp(pos.x,   -0.3f, 0.3f);
                pos.y = Mathf.Clamp(pos.y,  -0.3f, 0.3f);
                rightCoil.transform.position = pos;*/
			}

			

		}


		/*if (Input.GetKey (KeyCode.Mouse0) && ! Input.GetKey (KeyCode.Mouse1)) { //Left Hand
                Vector3 delta = Vector3.zero;
                delta.x=Input.GetAxis ("Mouse X") * Constnts.MOVEMENT_SPEED ;
                delta.y=Input.GetAxis ("Mouse Y") * Constnts.MOVEMENT_SPEED ;
                positionLeft += delta;
 
                // Bounds for the hand movenent
                Vector3 pos = positionLeft;
                pos.x = Mathf.Clamp(pos.x,   -0.3f, 0.3f);
                pos.y = Mathf.Clamp(pos.y,  -0.3f, 0.3f);
                positionLeft = pos;
 
            this.transform.parent.gameObject.SendMessage("TakeMana", takeMana);// Moving the hands takes off from manaBar
                source.PlayOneShot(moveSounds[0],0.5f); // Play left hand movement sound
 
 
            }
        if (Input.GetKey (KeyCode.Mouse1) && ! Input.GetKey (KeyCode.Mouse0)) { //Right Hand
                Vector3 delta = Vector3.zero;
                delta.x=Input.GetAxis ("Mouse X") * Constnts.MOVEMENT_SPEED ;
                delta.y=Input.GetAxis ("Mouse Y") * Constnts.MOVEMENT_SPEED ;
                positionRight += delta;
 
                // Bounds for the hand movenent
                Vector3 pos = positionRight;
                pos.x = Mathf.Clamp(pos.x, rightHandClampX,rightHandClampY);
                pos.y = Mathf.Clamp(pos.y,  -0.3f, 0.3f);
                positionRight = pos;
 
            this.transform.parent.gameObject.SendMessage("TakeMana", takeMana);// Moving the hands takes off from manaBar
                source.PlayOneShot(moveSounds[1],0.5f); // Play left hand movement sound
    
            }
        if (Input.GetKey (KeyCode.Mouse1) && Input.GetKey (KeyCode.Mouse0)) {//This is for the width of the plain outside
                plane.rotation *= Quaternion.AngleAxis (Input.GetAxis ("Mouse Y") * 1, plane.right);
                plane.rotation *= Quaternion.AngleAxis (Input.GetAxis ("Mouse X") * 1, plane.up);
                source.PlayOneShot(moveSounds[2],0.5f); // Play left hand movement sound
 
                //plane.rotation.eulerAngles.x//clamp/adding the angular in the tjum
            this.transform.parent.gameObject.SendMessage("TakeMana", takeMana);// Moving the hands takes off from manaBar
 
                }*/
	}

	void OnAnimatorIK()
	{
		float rhWeight = 1f;
		float lhWeight = 1f;

		anim.SetIKPosition (AvatarIKGoal.LeftHand,leftCoil.rB.position);
		anim.SetIKPositionWeight (AvatarIKGoal.LeftHand, lhWeight);

		anim.SetIKPosition (AvatarIKGoal.RightHand,rightCoil.rB.position);//targetLef
		anim.SetIKPositionWeight (AvatarIKGoal.RightHand, rhWeight);


		//Dealing with extraordinary body movements:
		//anim.SetIKHintPositionWeight (AvatarIKHint.LeftElbow, lhWeight);
		//anim.SetIKHintPosition (AvatarIKHint.LeftElbow    , hintLeft.position);

		//anim.SetIKRotation(AvatarIKGoal.LeftHand,leftCoil.rB.rotation);//targetLeft
		//anim.SetIKRotationWeight(AvatarIKGoal.LeftHand,lhWeight);
		/*
            if(rightHandIk){
                anim.SetIKPosition(AvatarIKGoal.RightHand, Coil.transform.position);
                anim.SetIKPositionWeight (AvatarIKGoal.RightHand, rhWeight);
 
                //Dealing with extraordinary body movements:
                anim.SetIKHintPositionWeight (AvatarIKHint.RightElbow, rhWeight);
                anim.SetIKHintPosition (AvatarIKHint.RightElbow    , hintRight.position);
 
                anim.SetIKRotation(AvatarIKGoal.RightHand,ikTargetRight.rotation);
                anim.SetIKRotationWeight(AvatarIKGoal.RightHand,rhWeight);
            }*/
		//}
	}
}
