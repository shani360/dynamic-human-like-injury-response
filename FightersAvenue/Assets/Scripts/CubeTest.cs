﻿using UnityEngine;
using System.Collections;

public class CubeTest : MonoBehaviour {
	Quaternion rotation;
	bool flag1;
	bool flag2;
	// Use this for initialization
	void Start () {
		flag1= false;
		flag2= false;
	}

	// Update is called once per frame
	void Update ()
	{
		//TEST NUMBER 1: Falling cube from above
		if (Input.GetKey (KeyCode.J) && !flag1) 
		{
			flag2 = false;
			GameObject  go;
			go = GameObject.CreatePrimitive(PrimitiveType.Cube);
			go.transform.localScale = new Vector3(0.5f, 0.5F, 0.5f);
			Rigidbody rb = go.AddComponent<Rigidbody>();
			Vector3 pos = new Vector3 (this.transform.position.x, this.transform.position.y+10f, this.transform.position.z);
			rb.MovePosition(pos);
			flag1 = true;
		}
		//TEST NUMBER 2: Hitting the cube with the hands
		if (Input.GetKey (KeyCode.K) && !flag2) 
		{
			flag1 = false;
			GameObject  go;
			go = GameObject.CreatePrimitive(PrimitiveType.Cube);
			go.transform.localScale = new Vector3(0.5f, 0.5F, 0.5f);
			Rigidbody rb = go.AddComponent<Rigidbody>();
			Vector3 pos = new Vector3 (this.transform.position.x-0.7f, this.transform.position.y+1.7f, this.transform.position.z+0.7f);
			rb.MovePosition(pos);
			rb.useGravity = false;
			flag2 = true;
		}
		//TEST NUMBER 3: Shooting the cube
		if(Input.GetKey (KeyCode.L)) 
		{
			GameObject  go;
			go = GameObject.CreatePrimitive(PrimitiveType.Cube);
			go.transform.localScale = new Vector3(0.5f, 0.5F, 0.5f);
			Rigidbody rb = go.AddComponent<Rigidbody>();
			rb.MovePosition(Camera.main.transform.position);
			Vector3 pos = Camera.main.transform.forward;
			pos.z = 10.0f;
			//go.transform.LookAt (Camera.main.ScreenToWorldPoint(pos));
			rb.AddRelativeForce(Vector3.forward*999);

		}

				//GameObject.Destroy (go);

		}

	}

