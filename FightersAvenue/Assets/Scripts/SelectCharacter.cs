﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;



public class SelectCharacter : MonoBehaviour {

	public Canvas myCanvas;
	List<GameObject> models;
	List<Sprite> images;
	public Transform myImages;

	int selectIndex; // Default index of the model
	int charactersCounter;
	bool isOne; // Change just one character
	// Use this for initialization
	void Start () {

		myCanvas.transform.GetChild (1).gameObject.SetActive (false); //The picking panel aside is disable

		models = new List<GameObject> ();
		images = new List<Sprite> ();

		selectIndex = 0;
		charactersCounter = 0;
		isOne = true;

		foreach (Transform t in transform)
		{
			models.Add (t.gameObject);
			t.gameObject.SetActive (false);
		}
		foreach (Transform t2 in myImages.transform)
		{
			images.Add (t2.transform.GetComponent<SpriteRenderer>().sprite);
		}

		models [selectIndex].SetActive (true);
	}
	void SelectModel()
	{


		models [selectIndex].SetActive (true);


	}

	public void Pick()
	{
		if(charactersCounter == 0) // First character
		{
			myCanvas.transform.GetChild (1).GetChild(1).GetComponent<Image>().sprite = images[selectIndex];
			myCanvas.transform.GetChild (1).gameObject.SetActive (true); //The picking panel aside is disable
			if(isOne)
				myCanvas.transform.GetChild (1).GetChild (6).gameObject.SetActive (false); //now can not go out
			else
				myCanvas.transform.GetChild (1).GetChild (6).gameObject.SetActive (true); //now can go out
			StaticsBool.player1Index = selectIndex;
			charactersCounter++;
			return;
		}
		if(charactersCounter == 1 && isOne) // Second character
		{
			myCanvas.transform.GetChild (1).GetChild(4).GetComponent<Image>().sprite = images[selectIndex];
			myCanvas.transform.GetChild (1).GetChild (6).gameObject.SetActive (true); //now can go out
			StaticsBool.player2Index = selectIndex;


			//charactersCounter++;
			return;
		}
		if (charactersCounter > 1) // Do nothing
		{
			return;
		}
	}
	public void Apply()
	{
		
		SceneManager.LoadScene ("Arena");
	}
	public void Back()
	{

		SceneManager.LoadScene ("MainMenu");
	}
	public void Change1()
	{
		if( charactersCounter == 1)
			isOne = false;
		else
			isOne = true;

		myCanvas.transform.GetChild (1).GetChild (6).gameObject.SetActive (false); //now can not go out
		myCanvas.transform.GetChild (1).GetChild(1).GetComponent<Image>().sprite = null;
		charactersCounter = 0;


	}
	public void Change2()
	{

		myCanvas.transform.GetChild (1).GetChild (6).gameObject.SetActive (false); //now can not go out
		myCanvas.transform.GetChild (1).GetChild(4).GetComponent<Image>().sprite = null;
		charactersCounter = 1;
		isOne = true;
	}

	public void Next()
	{
		models [selectIndex].SetActive (false);
		if (selectIndex < models.Count - 1)
			selectIndex++;
		else
			selectIndex = 0;

		SelectModel ();
	}
	public void Previous()
	{
		models [selectIndex].SetActive (false);
		if (selectIndex > 0)
			selectIndex--;
		else
			selectIndex = models.Count - 1;
		SelectModel ();
	}
}
