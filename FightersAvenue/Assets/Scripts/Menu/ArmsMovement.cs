﻿using UnityEngine;
using System.Collections;

public class ArmsMovement : MonoBehaviour {

	Animator anim;
	void Awake()
	{
		anim = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		LeftPunch ();
		RightPunch ();
	}
	void LeftPunch ()
	{
		if(Input.GetKey (KeyCode.Mouse0)) {
			anim.SetTrigger ("punchLeft");
		}
	}
	void RightPunch ()
	{
		if (Input.GetKey (KeyCode.Mouse1)) {
			anim.SetTrigger ("punchRight");
		}
	}
}
