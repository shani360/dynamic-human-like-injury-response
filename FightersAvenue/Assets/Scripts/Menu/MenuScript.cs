﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

public class MenuScript : MonoBehaviour {

	public Canvas quitMenu;
	public Canvas settingsMenu;
	public Button startText;
	public Button exitText;
	public Button SettingsText;

	// Use this for initialization
	void Start () {
		quitMenu = quitMenu.GetComponent<Canvas> ();
		startText = startText.GetComponent<Button> ();
		exitText = exitText.GetComponent<Button> ();
		quitMenu.enabled = false;
		settingsMenu.enabled = false;
	}
	
	public void ExitPress()
	{
		quitMenu.enabled = true;
		startText.enabled = false;
		exitText.enabled = false;
		SettingsText.enabled  = false;
		settingsMenu.enabled = false;

	}

	public void NoPress()
	{
		quitMenu.enabled = false;
		settingsMenu.enabled = false;

		SettingsText.enabled  = true;
		startText.enabled = true;
		exitText.enabled = true;
	}

	public void StartLevel()
	{
		SceneManager.LoadScene ("CharacterSelection"); // Start Game 
	}

	public void ExitGame ()
	{
		Application.Quit ();
	}

	public void SettingsPress()
	{
		settingsMenu.enabled = true;
		quitMenu.enabled = false;
		startText.enabled = false;
		exitText.enabled = false;
		SettingsText.enabled  = false;

	}

	public void PickCharacterPress()
	{
		SceneManager.LoadScene ("CharacterSelection"); // Go pick your character
	}
}
