﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayerAbove : MonoBehaviour {

	// This script is for adding to the camera the panel of the name, health and mana of the player.
	// The Panel also follows the player.

	public GameObject player;
	public Vector3 offset;

	bool isAssigned;

	// Initialization
	void Start () {
		player = null;
		isAssigned = false;
	}
	
	// Update is called once per frame
	void Update () {
		
		if (player)	// Havn't assigned the character that the player choose in the GameLogic script yet
			isAssigned = true;
		
		if(isAssigned)
			gameObject.transform.position = Camera.main.WorldToScreenPoint (player.transform.position) + offset;
	}
}
