﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {

	//public Image healthBar;//visual
	float hitPoint=90;
	float maxPoint=90;

	public Slider healthBar;
	public Slider ManaBar;


	/*void Start () 
	{
		healthBar.GetComponent<Slider> ().value -= 0.5f;
	}
	void UpdateHealthBar () 
	{
		float rate;
		rate = hitPoint / maxPoint;
		// change in the actual healthbar
		//healthBar.rectTransform.localScale = new Vector3 (rate, 1f, 1f);
	}*/
	public void TakeHealth(float damage)
	{
		/*hitPoint -= damage;

		if (hitPoint < 0) {
			hitPoint = 0;
			Debug.Log ("You lost in the fight!");
		}
		UpdateHealthBar ();*/

		healthBar.GetComponent<Slider> ().value -= damage;

	}

	public void TakeMana(float damage)
	{
		ManaBar.GetComponent<Slider> ().value -= damage;

	}

	public void HealMana()
	{
		ManaBar.GetComponent<Slider> ().value = 1f;


	}
	public bool isManaEmpty()
	{
		if (ManaBar.GetComponent<Slider> ().value == 0f)
			return true;
		return false;
	}

	public bool isHealthEmpty()
	{
		if (healthBar.GetComponent<Slider> ().value == 0f)
			return true;
		return false;
	}

}
/*
public float damage;
public bool isDamaging;
private void TrigerSay(Collider col)
{
	if(col.tag=="Player")
		col.SendMassage((isDamaging)?"TakeDamage":"HealDamage",Time.deltaTime*damage);
}*/