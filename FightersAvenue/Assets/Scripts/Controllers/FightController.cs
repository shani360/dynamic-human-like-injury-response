﻿using UnityEngine;
using System.Collections;

public class FightController : MonoBehaviour {
	Animator anim;
	public float RotateSpeed;

	void Awake()
	{
		anim = GetComponent<Animator> ();
	}
	// Update is called once per frame
	void Update () {

		Defense ();
		Jump ();
		Bend ();
		Turn ();
	
	}
	void Jump ()
	{
		if (Input.GetKey (KeyCode.Space)) {
			anim.SetBool ("Jump", true);
		} else {
			anim.SetBool ("Jump", false);

		}
	}
	void Defense()
	{
		if (Input.GetKey (KeyCode.E)) {
			anim.SetBool ("Defense", true);
		} else {
			anim.SetBool ("Defense", false);

		}
	}
	void Bend()
	{
		if (Input.GetKey (KeyCode.Q)) {
			anim.SetBool ("Bend", true);
		} else {
			anim.SetBool ("Bend", false);

		}
	}
	void Turn()
	{
		if( Input.GetKey( KeyCode.A ) || Input.GetAxisRaw("Horizontal") < -0.1f )
		{
			transform.Rotate(0.0f, -RotateSpeed * Time.deltaTime, 0.0f);
		}

		if( Input.GetKey( KeyCode.D ) || Input.GetAxisRaw("Horizontal") > 0.1f )
		{
			transform.Rotate(0.0f, RotateSpeed * Time.deltaTime, 0.0f);
		}
	}
}
