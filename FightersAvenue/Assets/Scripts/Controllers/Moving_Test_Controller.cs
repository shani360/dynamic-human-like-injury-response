﻿using UnityEngine;
using System.Collections;

public class Moving_Test_Controller : MonoBehaviour {

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		float pos=0;
		if(Input.GetAxis("Mouse X")<0 && Input.GetKey (KeyCode.W)){
			pos++;
			transform.Translate(0, pos, Time.deltaTime);
			//Code for action on mouse moving left
			print("Mouse moved left");
		}
		if(Input.GetAxis("Mouse X")>0 && Input.GetKey (KeyCode.W)){
			pos--;
			transform.Translate(0, pos, Time.deltaTime);
			//Code for action on mouse moving right
			print("Mouse moved right");
		}

	}
}
