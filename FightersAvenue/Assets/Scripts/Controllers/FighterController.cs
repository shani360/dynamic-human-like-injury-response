﻿using UnityEngine;
using System.Collections;

public class FighterController : MonoBehaviour {
	Animator anim;
	public float RotateSpeed;
	public bool isWin = false;
	public bool isLose= false;

	void Start()
	{
		RotateSpeed = 25;
	}
	void Awake()
	{
		anim = GetComponent<Animator> ();
	}
	// Update is called once per frame
	void Update () {

		//Defense ();
		//Jump ();
		//Bend ();
		Turn ();
		BoxRight ();
		BoxLeft ();
		//Win ();
		Lose ();

	}
	void BoxRight () //Make the box animation on the right hand
	{
		if (Input.GetMouseButton(1) )//&& Physics.Raycast() 
		{
			anim.SetBool ("Box", true);
		} 
		else {
			anim.SetBool ("Box", false);
		}
			
	}
	void BoxLeft () //Make the box animation on the left hand
	{
		if (Input.GetMouseButton(0))//&& Physics.Raycast()  
		{
			anim.SetBool ("Box2", true);
		} 
		else {
			anim.SetBool ("Box2", false);
		}
	}
	void Jump ()
	{
		if (Input.GetKey (KeyCode.Space)) {
			anim.SetBool ("Jump", true);
		} else {
			anim.SetBool ("Jump", false);

		}
	}
	void Defense()
	{
		if (Input.GetKey (KeyCode.E)) {
			anim.SetBool ("Defense", true);
		} else {
			anim.SetBool ("Defense", false);

		}
	}
	void Bend()
	{
		if (Input.GetKey (KeyCode.Q)) {
			anim.SetBool ("Bend", true);
		} else {
			anim.SetBool ("Bend", false);

		}
	}
	void Turn()
	{
		if (!Input.GetMouseButton (2)) {
			if (Input.GetKey (KeyCode.A) || Input.GetAxisRaw ("Horizontal") < -0.1f) {
				transform.Rotate (0.0f, -RotateSpeed * Time.deltaTime, 0.0f);
			}

			if (Input.GetKey (KeyCode.D) || Input.GetAxisRaw ("Horizontal") > 0.1f) {
				transform.Rotate (0.0f, RotateSpeed * Time.deltaTime, 0.0f);
			}
		}
	}
	/*void Win()
	{
		if (isWin) {
			anim.Play("Win");
			isWin = false;

		}
	}*/
	void Lose()
	{
		if (isLose) {
			anim.Play("Lose");
			//isLose = false;
		} 
	}


}
