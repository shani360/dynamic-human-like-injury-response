﻿using UnityEngine;
using System.Collections;

public class IK_Controller : MonoBehaviour {

	public bool use_ik=false;
	bool leftHandIk=false;
	bool rightHandIk=false;

	bool elbowIk=false;

	public Transform ikTargetRight;
	public Transform ikTargetLeft;

	public Rigidbody plain;

	public Transform hintLeft;
	public Transform hintRight;

	public Vector3 positionLeft;
	public Vector3 positionRight;

	public Quaternion leftHandRot;
	public Quaternion rightHandRot;

	Animator anim;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
	}
	// Update is called once per frame
	void Update () {
		plain.rotation = Quaternion.identity;
		plain.position = this.transform.forward;

	}
	void FixedUpdate()
	{
		Get_Hands_Positions ();//place the targets posiotions

		//leftHand
		if(Input.GetKey (KeyCode.Mouse0)) 
		{
			use_ik=true;
			leftHandIk = true;
			Mouse_IK ();
			//leftHandRot = Quaternion.FromToRotation (Vector3.forward,lHit.normal);
//			this.GetComponent<HealthBar> ().TakeDamage (0.01f);//Using IK takes off from manaBar

		}
		else 
		{
			leftHandIk = false;
		}

		//rightHand
		if(Input.GetKey (KeyCode.Mouse1)) 
		{
			//anim.SetBool ("Raise", true);// when we want to stop raising the hand we will change this to false
			use_ik=true;
			rightHandIk = true;
			Mouse_IK ();
			//rightHandRot = Quaternion.FromToRotation (Vector3.forward,rHit.normal);
//			this.GetComponent<HealthBar> ().TakeDamage (0.01f);//Using IK takes off from manaBar
		}
		else 
		{
			rightHandIk = false;
			//anim.SetBool ("Raise", false);
		}


		float count=ikTargetLeft.position.z;
		if (Input.GetKey (KeyCode.M)) //This is for the width of the plain inside
		{ 
			count -= 0.1f;

			if(ikTargetLeft)
				ikTargetLeft.position = new Vector3 (ikTargetLeft.position.x, ikTargetLeft.position.y, count);
			if(ikTargetRight)
				ikTargetRight.position = new Vector3 (ikTargetRight.position.x, ikTargetRight.position.y, count);
		} 
		if (Input.GetKey (KeyCode.N)) //This is for the width of the plain outside
		{ 
			count += 0.1f;

			if(ikTargetLeft)
				ikTargetLeft.position = new Vector3 (ikTargetLeft.position.x, ikTargetLeft.position.y, count);
			if(ikTargetRight)
				ikTargetRight.position = new Vector3 (ikTargetRight.position.x, ikTargetRight.position.y, count);
		} 
	}
	void OnAnimatorIK()
	{
		float rhWeight = 1f;
		float lhWeight = 1f;

		if (use_ik) {

			if(leftHandIk){

				anim.SetIKPosition (AvatarIKGoal.LeftHand, ikTargetLeft.position);
				anim.SetIKPositionWeight (AvatarIKGoal.LeftHand, lhWeight);

				//Dealing with extraordinary body movements:
				anim.SetIKHintPositionWeight (AvatarIKHint.LeftElbow, lhWeight);
				anim.SetIKHintPosition (AvatarIKHint.LeftElbow	, hintLeft.position);

				anim.SetIKRotation(AvatarIKGoal.LeftHand,ikTargetLeft.rotation);
				anim.SetIKRotationWeight(AvatarIKGoal.LeftHand,lhWeight);
			}
			if(rightHandIk){
				anim.SetIKPosition(AvatarIKGoal.RightHand, ikTargetRight.position);
				anim.SetIKPositionWeight (AvatarIKGoal.RightHand, rhWeight);

				//Dealing with extraordinary body movements:
				anim.SetIKHintPositionWeight (AvatarIKHint.RightElbow, rhWeight);
				anim.SetIKHintPosition (AvatarIKHint.RightElbow	, hintRight.position);

				anim.SetIKRotation(AvatarIKGoal.RightHand,ikTargetRight.rotation);
				anim.SetIKRotationWeight(AvatarIKGoal.RightHand,rhWeight);

			}
		}
	}
	//Current position of the hands
	void Get_Hands_Positions()
	{
		//find LefttHand position and follow it with the target
		GameObject ParentGameObjectLeft = GameObject.Find("char_ethan_LeftHand"); //IN THE TEST_IK WILL BE "LeftHand"//in ethan:"char_ethan_LeftHand"
		GameObject ChildGameObjectLeft = ParentGameObjectLeft.transform.GetChild (0).gameObject; 

		positionLeft = ChildGameObjectLeft.transform.position;

		ikTargetLeft.position = new Vector3 (positionLeft.x, positionLeft.y, positionLeft.z);

		//find RightHand position and follow it with the target
		GameObject ParentGameObjectRight = GameObject.Find("char_ethan_RightHand");  //IN THE TEST_IK WILL BE "RightHand"//in ethan:"char_ethan_RightHand"
		GameObject ChildGameObjectRight = ParentGameObjectRight.transform.GetChild (0).gameObject; 

		positionRight = ChildGameObjectRight.transform.position;

		ikTargetRight.position = new Vector3 (positionRight.x, positionRight.y, positionRight.z);
	}
	//Controling the hands with mouse action
	void Mouse_IK()
	{
		float posl=ikTargetLeft.position.y;
		float posr=ikTargetRight.position.y;

		float posu=ikTargetLeft.position.x;
		float posd=ikTargetLeft.position.x;

		if(Input.GetAxis("Mouse X")<0)//action on mouse moving left
		{
			print("Mouse moved left");
			if (leftHandIk) 
			{
				posl = posl + 0.1f;
				ikTargetLeft.position = new Vector3 (ikTargetLeft.position.x, posl, ikTargetLeft.position.z);
			}
			if (rightHandIk)
			{
				posr = posr + 0.1f;
				ikTargetRight.position = new Vector3 (ikTargetRight.position.x, posr, ikTargetRight.position.z);
			}
		}
		if(Input.GetAxis("Mouse X")>0)//action on mouse moving right
		{
			print("Mouse moved right");
			if (leftHandIk) 
			{
				posl = posl - 0.1f;
				ikTargetLeft.position = new Vector3 (ikTargetLeft.position.x, posl, ikTargetLeft.position.z);
			}
			if (rightHandIk)
			{
				posr = posr - 0.1f;
				ikTargetRight.position = new Vector3 (ikTargetRight.position.x, posr, ikTargetRight.position.z);
			}
		}
		////////////////////////////////////////UP DOWN///////////////////////////////////////////////////////////
		if(Input.GetAxis("Mouse Y")<0)//action on mouse moving up
		{
			print("Mouse moved down");
			if (leftHandIk) 
			{
				posd = posd + 0.1f;
				ikTargetLeft.position = new Vector3 (ikTargetLeft.position.x, posd, ikTargetLeft.position.z);
			}
			if (rightHandIk)
			{
				posd = posd + 0.1f;
				ikTargetRight.position = new Vector3 (ikTargetRight.position.x, posd, ikTargetRight.position.z);
			}
		}

		if(Input.GetAxis("Mouse Y")>0)//action on mouse moving right
		{
			print("Mouse moved up");
			if (leftHandIk) 
			{
				posu = posu - 0.1f;
				ikTargetLeft.position = new Vector3 (ikTargetLeft.position.x, posu, ikTargetLeft.position.z);
			}
			if (rightHandIk)
			{
				posu = posu - 0.1f;
				ikTargetRight.position = new Vector3 (ikTargetRight.position.x, posu, ikTargetRight.position.z);
			}
		}
	}

}
