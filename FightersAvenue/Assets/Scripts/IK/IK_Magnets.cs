﻿using UnityEngine;
using System.Collections;

public class IK_Magnets: MonoBehaviour {


	bool elbowIk=false;

	public Transform ikTargetRight;
	public Transform ikTargetLeft;

	//public Transform hintLeft;
	//public Transform hintRight;

	public Vector3 positionLeft;
	public Vector3 positionRight;

	public Quaternion leftHandRot;
	public Quaternion rightHandRot;

	Animator anim;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
	}
	// Update is called once per frame
	void Update () {

	}
	void OnAnimatorIK()
	{
		float rhWeight = 1f;
		float lhWeight = 1f;


				anim.SetIKPosition (AvatarIKGoal.LeftHand, ikTargetLeft.position);
				anim.SetIKPositionWeight (AvatarIKGoal.LeftHand, lhWeight);

				//Dealing with extraordinary body movements:
				/*anim.SetIKHintPositionWeight (AvatarIKHint.LeftElbow, lhWeight);
				anim.SetIKHintPosition (AvatarIKHint.LeftElbow	, hintLeft.position);*/

				anim.SetIKRotation(AvatarIKGoal.LeftHand,ikTargetLeft.rotation);
				anim.SetIKRotationWeight(AvatarIKGoal.LeftHand,lhWeight);

				anim.SetIKPosition(AvatarIKGoal.RightHand, ikTargetRight.position);
				anim.SetIKPositionWeight (AvatarIKGoal.RightHand, rhWeight);

				//Dealing with extraordinary body movements:
				/*anim.SetIKHintPositionWeight (AvatarIKHint.RightElbow, rhWeight);
				anim.SetIKHintPosition (AvatarIKHint.RightElbow	, hintRight.position);*/

				anim.SetIKRotation(AvatarIKGoal.RightHand,ikTargetRight.rotation);
				anim.SetIKRotationWeight(AvatarIKGoal.RightHand,rhWeight);

			

	}
	//Current position of the hands

	//Controling the hands with mouse action


}
