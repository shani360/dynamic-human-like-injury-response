﻿using System.Collections;
using UnityEngine;

static class Constants
{
	public const float DISTANCE = 1f;//distance between the character and the plain
	public const float MOVEMENT_SPEED=3.3f;

}

public class IK_Hands: MonoBehaviour {

	public Transform hintLeft;//The direction of the elbow
	public Transform targetLeft;
	public Transform hintRight;//The direction of the elbow
	public Transform targetRight;

	Vector3 positionLeft;
	Vector3 positionRight;

	//For connection with the coil://
	float x;
	float y;
	float z;

	bool isIk=false;
	bool isStartPos=true;
	///////////////// 

	bool flagPlane;//whether the plane of the forward


	IK_Coil coilScript;

	Animator anim;

	bool leftHandIk=false;
	bool rightHandIk=false;

	// Use this for initialization
	void Start () {

		flagPlane = false;
		isIk = false;

		Get_Hands_Positions ();

		anim = GetComponent<Animator> ();

	}

	// Update is called once per frame
	void Update () {
		Get_Hands_Positions ();

		if (!isIk) {
			targetLeft.transform.position = positionLeft;
			targetRight.transform.position = positionRight;
		}
		///////////////////////////////////////////////////
		/// 
	}

	void FixedUpdate()
	{
		//if (Input.GetMouseButtonUp (0) || Input.GetMouseButtonUp (1)) {
		if((! Input.GetKey (KeyCode.Mouse0) )||(!Input.GetKey (KeyCode.Mouse1))){
			isIk = false;
		}

		if (Input.GetKey (KeyCode.Mouse0)) { //Left Hand
			isIk = true;
			leftHandIk = true;
			isStartPos = false;
			if (Input.GetKey (KeyCode.LeftControl) || Input.GetKey (KeyCode.RightControl)) {//This is for the width of the plain outside
				flagPlane = true;
			} else
				flagPlane = false;
			Get_Hands_Positions ();
//			this.GetComponent<HealthBar> ().TakeDamage (0.01f);//Using IK takes off from manaBar
			targetLeft.position=MousePos (targetLeft.position);
		}
		else {
			leftHandIk = false;
		}
		if (Input.GetKey (KeyCode.Mouse1)) //Rigth Hand
		{
			isIk = true;
			rightHandIk = true;
			isStartPos = false;
			if (Input.GetKey (KeyCode.LeftControl) || Input.GetKey (KeyCode.RightControl))//This is for the width of the plain outside
			{
				flagPlane = true;
			}
			else
				flagPlane = false;

			Get_Hands_Positions ();

//			this.GetComponent<HealthBar> ().TakeDamage (0.01f);//Using IK takes off from manaBar

			targetRight.position=MousePos (targetRight.position);
		}
		else {
			rightHandIk = false;
		}
	}

	Vector3 MousePos(Vector3 pos)//ok 2 things:1)how about streng-if i go left one time but very very hard? 2)hasamim
	{
		if (Input.GetAxis ("Mouse X") < 0) {//action on mouse moving left
			pos+=Vector3.left * Constnts.MOVEMENT_SPEED;

			if (flagPlane)
			{
				pos+=Vector3.forward  * Constnts.MOVEMENT_SPEED;
			}
		}

		if (Input.GetAxis ("Mouse X") > 0) {//action on mouse moving right
			pos+=Vector3.right  * Constnts.MOVEMENT_SPEED;

			if (flagPlane){//Amur lesovev oto eichsheu
				//				plain.rotation = plain.transform.up;
				pos-=Vector3.forward  * Constnts.MOVEMENT_SPEED;
			}
		}

		if (Input.GetAxis ("Mouse Y") < 0) {//action on mouse moving down
			pos+=Vector3.down * Constnts.MOVEMENT_SPEED;

			if (flagPlane) {//Amur lesovev oto eichsheu
				//				plain.rotation = plain.transform.right;
				pos -= Vector3.forward * Constnts.MOVEMENT_SPEED;
			}
		}

		if (Input.GetAxis ("Mouse Y") > 0) {//action on mouse moving up
			pos+=Vector3.up  * Constnts.MOVEMENT_SPEED;

			if (flagPlane) {//Amur lesovev oto eichsheu
				//				plain.rotation = plain.transform.right
				pos += Vector3.forward * Constnts.MOVEMENT_SPEED;
			}
		}
		return pos;
	}

	void Get_Hands_Positions()
	{
		//find LefttHand position and follow it with the target
		GameObject ParentGameObjectLeft = GameObject.Find("char_ethan_LeftHand"); //IN THE TEST_IK WILL BE "LeftHand"//in ethan:"char_ethan_LeftHand"
		GameObject ChildGameObjectLeft = ParentGameObjectLeft.transform.GetChild (0).gameObject; 

		GameObject ParentGameObjectElbowLeft = GameObject.Find("char_ethan_LeftForeArm"); 
		GameObject ChildGameObjectElbowLeft = ParentGameObjectLeft.transform.GetChild (0).gameObject; 

		//print (ChildGameObjectLeft.transform.position);

		positionLeft = ChildGameObjectLeft.transform.position;


		//find RightHand position and follow it with the target
		GameObject ParentGameObjectRight = GameObject.Find("char_ethan_RightHand");  //IN THE TEST_IK WILL BE "RightHand"//in ethan:"char_ethan_RightHand"
		GameObject ChildGameObjectRight = ParentGameObjectRight.transform.GetChild (0).gameObject; 

		GameObject ParentGameObjectElbowRight = GameObject.Find("char_ethan_RightForeArm"); 
		GameObject ChildGameObjectElbowRight = ParentGameObjectLeft.transform.GetChild (0).gameObject;

		positionRight = ChildGameObjectRight.transform.position;


		if (!isIk) {
			hintLeft.rotation = ChildGameObjectElbowLeft.transform.rotation;
			hintLeft.position = ChildGameObjectElbowLeft.transform.position+new Vector3(-0.5f,0.1f,-0.1f);

			hintRight.rotation = ChildGameObjectElbowRight.transform.rotation;
			hintRight.position = ChildGameObjectElbowRight.transform.position+new Vector3(0.5f,0.1f,-1.5f);
		}

	}

	void OnAnimatorIK()
	{
		float rhWeight = 1f;
		float lhWeight = 1f;

		if (isIk) {

			if(leftHandIk){

				anim.SetIKPosition (AvatarIKGoal.LeftHand, targetLeft.transform.position);//targetLeft
				anim.SetIKPositionWeight (AvatarIKGoal.LeftHand, lhWeight);

				//Dealing with extraordinary body movements:
				anim.SetIKHintPositionWeight (AvatarIKHint.LeftElbow, lhWeight);
				anim.SetIKHintPosition (AvatarIKHint.LeftElbow	, hintLeft.position);

				anim.SetIKRotation(AvatarIKGoal.LeftHand,targetLeft.transform.rotation);//targetLeft
				anim.SetIKRotationWeight(AvatarIKGoal.LeftHand,lhWeight);
			}

			if(rightHandIk){
				anim.SetIKPosition(AvatarIKGoal.RightHand, targetRight.transform.position);
				anim.SetIKPositionWeight (AvatarIKGoal.RightHand, rhWeight);

				//Dealing with extraordinary body movements:
				anim.SetIKHintPositionWeight (AvatarIKHint.RightElbow, rhWeight);
				anim.SetIKHintPosition (AvatarIKHint.RightElbow	, hintRight.position);

				anim.SetIKRotation(AvatarIKGoal.RightHand,targetRight.rotation);
				anim.SetIKRotationWeight(AvatarIKGoal.RightHand,rhWeight);
			}
		}
	}
}