﻿using UnityEngine;
using System.Collections;

public class IK_Test : MonoBehaviour {

	public bool use_ik;
	public bool leftHandIk;
	public bool rightHandIk;

	public Vector3 leftHandPos;
	public Vector3 rightHandPos;

	public Quaternion leftHandRot;
	public Quaternion rightHandRot;

	Animator anim;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
	}

	void FixedUpdate()
	{
		RaycastHit lHit;
		RaycastHit rHit;

		//leftHand
		if (Physics.Raycast (transform.position + new Vector3 (0.0f, 2.0f, 0.5f), -transform.up + new Vector3 (-0.5f, 0.0f, 0.0f), out lHit, 1f)) {
			leftHandIk = true;
			leftHandPos = lHit.point;
			leftHandRot = Quaternion.FromToRotation (Vector3.forward,lHit.normal);
		}
		else {
			leftHandIk = false;
		}

		//rightHand
		if (Physics.Raycast (transform.position + new Vector3 (0.0f, 2.0f, 0.5f), -transform.up + new Vector3 (0.5f, 0.0f, 0.0f), out rHit, 1f)) {
			rightHandIk = true;
			rightHandPos = rHit.point;
			rightHandRot = Quaternion.FromToRotation (Vector3.forward,rHit.normal);
		}
		else {
			rightHandIk = false;
		}
	}
	// Update is called once per frame
	void Update () {
		//left hand visual ray
		Debug.DrawRay (transform.position + new Vector3 (0.0f, 2.0f, 0.5f), -transform.up + new Vector3 (-0.5f, 0.0f, 0.0f), Color.green);
		//right hand visual ray
		Debug.DrawRay (transform.position + new Vector3 (0.0f, 2.0f, 0.5f), -transform.up + new Vector3 (0.5f, 0.0f, 0.0f), Color.yellow);
	}
	void OnAnimatorIK()
	{
		if (use_ik) {
			if(leftHandIk){
				anim.SetIKPosition (AvatarIKGoal.LeftHand, leftHandPos);
				anim.SetIKPositionWeight (AvatarIKGoal.LeftHand, 1f);

				anim.SetIKRotation (AvatarIKGoal.LeftHand, leftHandRot);
				anim.SetIKRotationWeight (AvatarIKGoal.LeftHand, 1f);
			}
			if(rightHandIk){
				anim.SetIKPosition (AvatarIKGoal.RightHand, rightHandPos);
				anim.SetIKPositionWeight (AvatarIKGoal.RightHand, 1f);

				anim.SetIKRotation (AvatarIKGoal.RightHand, rightHandRot);
				anim.SetIKRotationWeight (AvatarIKGoal.RightHand, 1f);
			}
		}
	}
}
