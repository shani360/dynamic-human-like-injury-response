//
var xSpeed = 0.0; //init in editor
var ySpeed = 0.0;
var angleAccelaration = 0.0;

var yMinLimit = -60;
var yMaxLimit = 80;

private var x = 0;
private var y = 0;
private var goalAngle = Quaternion.Identity;
private var X_SPEED_LIMIT = 10;
private var Y_SPEED_LIMIT = 4;

@script AddComponentMenu("Camera-Control/Mouse Orbit")

function Start () {
    var angles = transform.eulerAngles;
    x = 0;//angles.y;
    y = 0;//angles.x;
    goalAngle = transform.rotation;
}

function Update () {
    if(!Input.GetMouseButton(0) && !Input.GetMouseButton(1) && Input.GetMouseButton(2)) {
        x = Input.GetAxis("Mouse X") * xSpeed;
        y = Input.GetAxis("Mouse Y") * ySpeed;
        x = Mathf.Clamp(x, -X_SPEED_LIMIT, X_SPEED_LIMIT);
        y = Mathf.Clamp(y, -Y_SPEED_LIMIT, Y_SPEED_LIMIT);
        //y = ClampAngle(y, yMinLimit, yMaxLimit);
        hor = Quaternion.AngleAxis(x, Vector3.up);
        vert = Quaternion.AngleAxis(-y, goalAngle*Vector3.right);
        goalAngle = hor * vert * goalAngle;
        goalAngle = ZeroRoll(goalAngle);
    }
    if(!Input.GetMouseButton(2)) {
        goalAngle = Quaternion.Slerp(goalAngle, transform.rotation, 0.07f);
        goalAngle = ZeroRoll(goalAngle);
        //var angles = goalAngle.eulerAngles;
        //x = angles.y;
        //y = angles.x;
        //goalAngle = Quaternion.Euler(y, x, 0);
    }
    transform.rotation = Quaternion.RotateTowards(transform.rotation, goalAngle, Quaternion.Angle(transform.rotation, goalAngle)*Time.deltaTime*angleAccelaration/Time.timeScale);
    transform.rotation = ZeroRoll(transform.rotation);
}
static function ZeroRoll(q : Quaternion) {
    desiredRight = Vector3.Cross(q * Vector3.forward, Vector3.up);
    desiredForward = q * Vector3.forward;
    if(desiredRight.magnitude < 0.1f)
    {
        desiredForward += Vector3.Cross(Vector3.up, (0.1f-desiredRight.magnitude)*desiredRight.normalized);
    }
    desiredUp = Vector3.Cross(desiredRight, desiredForward);
    q = Quaternion.LookRotation(desiredForward, desiredUp);
    return q;
}
