﻿using UnityEngine;
using System.Collections;

public class cameraObserver : MonoBehaviour {

	//initial speed
	public int defaultSpeed = 7;
	private Vector3 currSpeed;
	private float accelaration = 3f;
	public Transform lookAt;
	Vector3 prevPos;
	// Use this for initialization
	void Awake () {
		this.transform.position = lookAt.position;
		prevPos = lookAt.position;

	}

	// Update is called once per frame
	void Update () {
		
		//this.transform.rotation = lookAt.rotation;

		float speed = defaultSpeed / Time.timeScale;
		//press shift to move faster
		if(Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
			speed *= 3; 
		Vector3 dir = Vector3.zero;

		if(Input.GetKey(KeyCode.A) && Input.GetMouseButton(2))
			dir -= transform.right;

		if(Input.GetKey(KeyCode.S) && Input.GetMouseButton(2))
			dir -= transform.forward;

		if(Input.GetKey(KeyCode.D) && Input.GetMouseButton(2))
			dir += transform.right;

		if(Input.GetKey(KeyCode.W) && Input.GetMouseButton(2))
			dir += transform.forward;

		if(Input.GetKey(KeyCode.E) && Input.GetMouseButton(2))
			dir += transform.up;

		if(Input.GetKey(KeyCode.Q) && Input.GetMouseButton(2))
			dir -= transform.up;
		
		Vector3 goalSpeed = dir * speed;

		//what to add currSpeed to make it closer to goalSpeed
		Vector3 speedDelta = (goalSpeed-currSpeed)*accelaration/Time.timeScale*Time.deltaTime;
		currSpeed += speedDelta;
		//if adding it passed goalSpeed, go back to assign goalSpeed
		if(Vector3.Dot(speedDelta, goalSpeed-currSpeed) < 0)
			currSpeed = goalSpeed;
				
		transform.position += currSpeed * Time.deltaTime;
	}
	public void FixedUpdate()
	{
		this.transform.position += lookAt.position - prevPos;
		prevPos = lookAt.position;
	}
}
