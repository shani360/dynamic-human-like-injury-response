# README #

# Dynamic Human Like Control and Response

* This project is being written in Unity environment.
* In this project,there will be use in IK technology and the simulation of the human body by:
     * The demonstration of a character dealing with physical injuries.
     * Letting the user have a real-time motion control inside a dynamic environment while keeping a physical human reliability.
     * Response to injuries in Real-time with a reliable physiological standards.




## [Issue Board](https://bitbucket.org/shani360/dynamic-human-like-injury-response/issues?status=new&status=open)

## Disclaimer
This project is developed as part of a final project at the software engineering department - [Azrieli College of Engineering](http://www.jce.ac.il/), Jerusalem, Israel.

### [User Manual](../../wiki/user-manual) (empty)

### Project Documents:

### Other Pages: